import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { visitFunctionBody } from 'typescript';

@Component({
  selector: 'app-multiply',
  templateUrl: './multiply.component.html',
  styleUrls: ['./multiply.component.css']
})
export class MultiplyComponent implements OnInit {

  num1: number = null;
  num2: number = null;
  num3 = null;
  num4: number = null;
  num5: number = null;

  formy = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  num(e): boolean{
    const charCode = (e.which) ? e.which : e.keycode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }
    return true;
  }

  change1(): void{
    this.num1 = this.formy.get('num1').value;
  }
  change2(): void{
    this.num2 = this.formy.get('num2').value;
  }

  multSbmt(): void{
    let bobject = new FormData();
    bobject.append('x', this.formy.get('num1').value);
    bobject.append('y', this.formy.get('num2').value);

    this.http.post(`http://localhost:8080/math/multiply`, bobject,
    {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe(
      ccc => {
        this.num3 = ccc;
      },
      (bad: HttpErrorResponse) => {
        console.log("error: " + bad.error);
        console.log("name: " + bad.name);
        console.log("message: " + bad.message);
        console.log("status: " + bad.status);
      }
    );

    this.num4 = this.formy.get('num1').value;
    this.num5 = this.formy.get('num2').value;
    this.formy.reset();
  }
}
