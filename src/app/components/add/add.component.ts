import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  num1: number = null;
  num2: number = null;
  num3 = null;
  num4: number = null;
  num5: number = null;

  formy = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  num(e): boolean{
    const charCode = (e.which) ? e.which : e.keycode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }

    return true;
  }

  change1(): void{
    this.num1 = this.formy.get('num1').value;
  }
  change2(): void{
    this.num2 = this.formy.get('num2').value;
  }

  addSbmt(): void{
    this.http.get(`http://localhost:8080/math/add?x=
    ${this.formy.get('num1').value}&y=${this.formy.get('num2').value}`)
    .toPromise().then((aaa) => {
      this.num3 = aaa;
    });

    this.num4 = this.formy.get('num1').value;
    this.num5 = this.formy.get('num2').value;
    this.formy.reset();
  }
}
