import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-subtract',
  templateUrl: './subtract.component.html',
  styleUrls: ['./subtract.component.css']
})
export class SubtractComponent implements OnInit {

  num1: number = null;
  num2: number = null;
  num3 = null;
  num4: number = null;
  num5: number = null;

  formy = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  num(e): boolean{
    const charCode = (e.which) ? e.which : e.keycode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }
    return true;
  }

  change1(): void{
    this.num1 = this.formy.get('num1').value;
  }
  change2(): void{
    this.num2 = this.formy.get('num2').value;
  }

  subSub(): void{
    // this.num3 = null;
    this.http.get(`http://localhost:8080/math/subtract/${this.formy.get('num1').value}/${this.formy.get('num2').value}`)
    .toPromise().then((bbb) => {
      this.num3 = bbb;
    });

    this.num4 = this.formy.get('num1').value;
    this.num5 = this.formy.get('num2').value;
    this.formy.reset();
  }
}
