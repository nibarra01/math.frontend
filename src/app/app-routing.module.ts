import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './components/add/add.component';
import { MathComponent } from './components/math/math.component';
import { MultiplyComponent } from './components/multiply/multiply.component';
import { SubtractComponent } from './components/subtract/subtract.component';

const routes: Routes = [
  {
    path: 'add',
    component: AddComponent
  },
  {
    path: 'subtract',
    component: SubtractComponent
  },
  {
    path: 'multiply',
    component: MultiplyComponent
  },
  {
    path: '',
    component: MathComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
